<?php

namespace Laratube;
use Laratube\Model;
use Laratube\User;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Apatie\MediaLibrary\HasMedia\HasMediaTrait;


class Channel extends Model implements HasMedia
{
	use HasMediaTrait;

	//user relationship
  public function user()
  {
  	return $this->belongsTo(User::class);
  }

}
